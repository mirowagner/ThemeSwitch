# ThemeSwitch

A small (>1KB gzipped), simple Javascript plugin to help manage multiple CSS themes.

## Example usage:

### File structure:
~~~
css/
	themes/
		light.css
		dark.css
~~~

### Instantiate:
~~~
var themeSwitch = new ThemeSwitch({
	basePath: '/css/themes/',
	defaultTheme: 'light'
});
~~~

### Switch theme:
~~~
themeSwitch.setTheme('dark');
~~~

## Config:

| Option | Type | Default | Description |
|---|---|---|---|
| `directory` | string | `'/'` | The directory containing the themes to switch between. |
| `defaultTheme` | string |  | The theme to set as default. |
| `linkId` | string |  | Set the link element's `id`.
| `debugMode` | boolean |  | Log errors. |



## Methods:
| Method | Description |
|---|---|
| `setTheme(theme)` | Switch to `theme`. If `theme` not provided, use `defaultTheme`. |
| `create()` | Create link element. Automatically runs when instantiated. |
| `destroy()` | Destroy link element. |
