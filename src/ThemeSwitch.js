/** Class representing a theme switch. */
class ThemeSwitch {
	/**
	 * Create a theme switch instance.
	 * @param {object} opts
	 */
	constructor({
		debugMode = false,
		directory = '/',
		defaultTheme,
		linkId
	}) {
		// If directory lacks trailing slash, add one.
		if (directory.slice(-1) !== '/') {
			directory += '/';
		}
		this.debugMode = debugMode;
		this.directory = directory;
		this.defaultTheme = defaultTheme;
		this.linkId = linkId;
		this.create();
	}

	/**
	 * Create link element.
	 * @throws {object} Error - if link element doesn't exist.
	 * @returns {boolean} Representing success or failure.
	 */
	create() {
		if (!this.elThemeLink) {
			// Create link element in memory.
			this.elThemeLink = document.createElement('link');
			this.elThemeLink.rel = 'stylesheet';
			if (this.linkId) {
				this.elThemeLink.id = this.linkId;
			}

			// Append link element to document head.
			document.head.appendChild(this.elThemeLink);

			// Switch to default theme.
			if (this.defaultTheme) {
				this.setTheme(this.defaultTheme);
			}

			return true;

		} else {
			if (this.debugMode) {
				throw new Error('ThemeSwitch: Link element already exists.');
			}
			return false;
		}
	}

	/**
	* Destroy link element.
	* @throws {object} Error - if link element doesn't exist.
	* @returns {boolean} Representing success or failure.
	*/
	destroy() {
		if (this.elThemeLink) {
			// Remove from DOM.
			this.elThemeLink.remove();

			// Delete from this
			delete this.elThemeLink;
			return true;

		} else {
			if (this.debugMode) {
				throw new Error('ThemeSwitch: Nothing to destroy.');
			}
			return false;
		}
	}

	/**
	 * Change the active theme.
	 * @arg {string} theme - Uses default theme if empty.
	 * @throws {object} Error - if link element doesn't exist.
	 * @returns {boolean} Representing success or failure.
	 */
	setTheme(theme) {
		if (this.elThemeLink) {
			if (!theme) {
				theme = this.defaultTheme;
			}
			this.elThemeLink.href = `${this.directory + theme}.css`;
			return true;

		} else {
			if (this.debugMode) {
				throw new Error('ThemeSwitch: Link element doesn\'t exist.');
			}
			return false;
		}
	}
}
