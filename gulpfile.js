const gulp = require('gulp');
const babel = require('gulp-babel');
const jsdoc = require('gulp-jsdoc3');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');

gulp.task('default', () => {
  return gulp.src('./src/ThemeSwitch.js')
    .pipe(jsdoc({
      opts: {
        destination: 'docs',
        readme: 'README.md'
      },
      templates: {
        theme: 'cosmo'
      }
    }))
    .pipe(sourcemaps.init())
    .pipe(babel({
        presets: ['es2015']
    }))
    .pipe(rename({extname: '.min.js'}))
    .pipe(uglify())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./dist/'));
});

gulp.task('watch', ['default'], () => {
	gulp.watch('./src/**/*.js', ['default']);
});
